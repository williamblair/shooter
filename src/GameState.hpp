#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

#include "State.hpp"

#include <vector>

#include "Spawner.hpp"

// BJ added music include
#include <SFML/Audio/Music.hpp>

class GameObject;

class GameState : public State
{
private:

	Spawner * m_spawner;

	float m_lastFire = 0.0f;

	GameObject * m_player = nullptr;

	std::vector<GameObject *> m_enemies;

	std::vector<GameObject *> m_bullets;

	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const;

	void updateCollision();

	// BJ added sound effect
	sf::Music m_fireSound;
	bool m_fireSoundLoaded = true;

public:
	
	GameState();

	~GameState();

	void update(float delta);

};

#endif
